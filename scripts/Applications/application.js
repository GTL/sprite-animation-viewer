//JaHOVA OS Instance
var os = null; 

//To Do
// On Animation delete, remove from Sprite
// Add on fullscreen to center sprite
// Make path non editable on ss
//
// JSON exporter

// Update onDropdown in Base App

//
//  Entry Method Called from OS
//
Application = function(){
    //Get pionter to JaHOVA OS
    os = com.jahova.os.Instance();
    
    //Connect HTML Objects to JavaScript
    App.Load();
    
    //App Initializtion
    App.Init();
    
}

//
//  Application Object Definition
//
var App = {
    Load: function(){
        App.HTML.container = document.getElementById('app');
    
        App.HTML.Main.container = document.getElementById('main');
        App.HTML.Main.window = document.getElementById('main-window');
        App.HTML.Main.footer = document.getElementById('main-footer');
        
        App.HTML.Thumnails.container = document.getElementById('thumbnail');
        App.HTML.Thumnails.window = document.getElementById('thumbnail-window');
        App.HTML.Thumnails.footer = document.getElementById('thumbnail-footer');
        
        App.HTML.Properties.container = document.getElementById('properties');
        App.HTML.Properties.header = document.getElementById('properties-header');
        App.HTML.Properties.window = document.getElementById('properties-window');
        App.HTML.Properties.dropDown = document.getElementById("dropdown");
        App.HTML.Properties.dropDown.onchange = function(e){
            var win = os.windows.WindowsManager.Windows.get("jahova.window.id." + e.currentTarget.value);
            win.MakeActive();
            if(App.Events.onDropdown){App.Events.onDropdown(win);};
        }
    },
    Display: {
        State: 'windowed',
        Toggle: function(){
            App.Display.State == "windowed" ? App.Display.Fullscreen() : App.Display.Windowed();
        },
        Fullscreen: function(){
            App.Display.State = "fullscreen";
            
            //Scroll Screen to top left, to hide any overflow
            scrollTo(0,0);
            
            App.HTML.Thumnails.container.style.display = "none";
            
            App.HTML.Properties.container.style.display = "none";
            
            //var win = document.getElementById("main-window");
            App.HTML.Main.window.setAttribute("class", "fullscreen");
            App.HTML.Main.window.style.height = window.innerHeight + "px";
            
            document.body.style.overflow = "hidden";
            
            //Hide Debug Bar, to make sure its not causing any problems
            os.debugbar.Disable();
            
            if(App.Events.onFullscreen){App.Events.onFullscreen()}
        },
        Windowed: function(){
            App.Display.State = "windowed";
            
            App.HTML.Thumnails.container.style.display = "";
            
            App.HTML.Properties.container.style.display = "";
            
            App.HTML.Main.window.setAttribute("class", "main-window border-radius-top-10px");
            App.HTML.Main.window.style.height = "";
            
            document.body.style.overflow = "";
            
            //Hide Debug Bar, to make sure its not causing any problems
            os.debugbar.Enable();
            
            if(App.Events.onWindowed){App.Events.onWindowed()}
        },
        EnableButton: function(){
            App.HTML.Main.window.innerHTML =  '<div id="expand-button" class="expand-button-min" onclick="App.Display.Toggle()"></div>'
        }
    },
    HTML: {
        container: null,
        Main: {
            container: null,
            window: null,
            footer: null
        },
        Properties: {
            container: null,
            header: null,
            window: null,
            dropDown: null
        },
        Thumnails: {
            container: null,
            window: null,
            footer: null
        }
    },
    FileIO: {
        Add: {
            FileSelector:{
                Main: function(label){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    App.HTML.Main.footer.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t\t<input type="file" name="mainFile" id="mainFile" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t</form>';
                    document.getElementById("mainFile").addEventListener("change", App.FileIO._Events.onFileLoaded);
                },
                Thumbnails: function(label){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    App.HTML.Thumnails.footer.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t<input type="file" name="ThumbnailFile" id="thumbnailsFile" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t\t</form>';
                    document.getElementById("thumbnailsFile").addEventListener("change", App.FileIO._Events.onFileLoaded);
                },
                Div: function(div, label, name){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    div.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t<input type="file" name="'+ name +'" id="'+ name +'" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t\t</form>';
                    
                    document.getElementById(name).addEventListener("change", App.FileIO._Events.onFileLoaded);
                }
            },
            Dropzone: {
                App: function(){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                
                    document.body.addEventListener("drop", App.FileIO._Events.onFileLoaded);
                    document.body.addEventListener("dragover", App.FileIO._Events.onFileDragOver);
                },
                Div: function(div){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    div.addEventListener("drop", App.FileIO._Events.onFileLoaded);
                    div.addEventListener("dragover", App.FileIO._Events.onFileDragOver);
                }
            }
        },
        Get: {
            Extension: function(file){
                return file.name.substr(file.name.lastIndexOf(".") + 1).toUpperCase();
            }
        },
        Read: {
            Text: function(file, callback, scope){
                var reader = new FileReader();
    
                reader.onload = function(e){
                    scope ? callback.apply(scope, [e.target.results]) : callback(e.target.result) ;
                }
                
                reader.readAsText(file);
            },
            Image: function(file, callback, scope){
                var reader = new FileReader();
                
                reader.onload = function(e){
                    img = new Image();
                    img.src = e.target.result;
                    scope ? callback.apply(scope, [img]) : callback(img) ;
                }
                
                reader.readAsDataURL(file);
            },
            DataURL: function(file, callback, scope){
                var reader = new FileReader();
                
                reader.onload = function(e){
                    scope ? callback.apply(scope, [e.target.result]) : callback(e.target.result) ;
                }
                
                reader.readAsDataURL(file);
            },
            ObjectURL: function(file){
                if ( window.webkitURL ) {
                    return window.webkitURL.createObjectURL( file );
                }
                else if ( window.URL && window.URL.createObjectURL ) {
                    return window.URL.createObjectURL( file );
                }
                else {
                    return null;
                }
            }
            
        },
        _Events: {
            onFileLoaded: function(e){
                e.stopPropagation();
                e.preventDefault();
                
                var files = e.target.files ? e.target.files : e.dataTransfer.files;
                var totalBytes = 0;
                
                for(var i = 0; i < files.length; i++){
                    var fileInfo = "File Name: " + files[i].name + "; Size: " + files[i].size + "; Type: " + files[i].type ;
                    totalBytes += files[i].size;
                    os.console.AppendComment(fileInfo);
                }
                
                os.console.Comment( "\nTotal of " + files.length + " files, " + totalBytes + " bytes.");
                
                App.Events.onFileLoaded(files);
            },
            onFileDragOver: function(e){
                e.stopPropagation();
                e.preventDefault();
            }
        }
    },
    Window: {
        Create: function(sTitle, sType, bDocked){
            var win = os.windows.WindowsManager.Create.Window(sTitle, sType);
            
            win.Hide.toolbar();
            win.Hide.menubar();
            win.elements.content.html().style.overflow = "hidden";
            win.Set.statusbarText("");
            win.Display.window();
            win.elements.titlebar.buttons.close.html().onclick = function(e){
                App.Window.Dock(os.windows.WindowsManager.Windows.get(e.target.id));    
            }
            win.onDock = function(){};
            win.onUndock = function(){};
            
            if(bDocked){App.Window.Dock(win);}
            
            return win;
        },
        Dock: function(win){

            win.Set.position(0,0);
            win.Set.width(358);
            win.Set.height(228);
            win.Hide.menubar();
            win.Hide.titlebar();
            win.Hide.statusbar();
            if(win.theme.name == "PC"){
                win.elements.window.Class.Remove("pcWindow ");
                win.elements.window.Class.Add("pcWindow-NoShadow");
            }
            else{
                win.elements.window.Class.Remove("macWindow ");
                win.elements.window.Class.Add("macWindow-NoShadow");
            }
            
            document.body.removeChild(win.elements.window.html());
            App.HTML.Properties.window.appendChild(win.elements.window.html());
            
            var idString = win.Get.id();
            idString = idString.split(".");
            
            var opt = document.createElement("option");
            opt.value = idString[idString.length - 1];
            opt.innerHTML = win.Get.title();
            
            App.HTML.Properties.dropDown.add(opt);
            
            App.HTML.Properties.dropDown.selectedIndex = opt.index;
            if(win.onDock){win.onDock();}
            
        },
        UnDock: function(e){
            if(App.HTML.Properties.dropDown.length > 0){
                var win = win = os.windows.WindowsManager.Windows.get("jahova.window.id." + App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex].value);
        
                win.Set.position(100,100);
                win.Set.width(358);
                win.Set.height(228);
                win.Display.titlebar();
                win.Display.statusbar();
                win.elements.window.html().style.position = "absolute";
                if(win.theme.name == "PC"){
                    win.elements.window.Class.Remove("pcWindow-NoShadow ");
                    win.elements.window.Class.Add("pcWindow");
                }
                else{
                    win.elements.window.Class.Remove("macWindow-NoShadow ");
                    win.elements.window.Class.Add("macWindow");
                }
                win.MakeActive();
                if(win.onUndock){win.onUndock();}
                
                App.HTML.Properties.window.removeChild(win.elements.window.html());
                document.body.appendChild(win.elements.window.html());
            
                App.HTML.Properties.dropDown.remove(App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex]);
                
                if(App.HTML.Properties.dropDown.length > 0){
                    win = os.windows.WindowsManager.Windows.get("jahova.window.id." + App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex].value);
                    win.MakeActive();
                }
                
                
            }
        }
    },
    Events: {
        onDropdown: function(win){
            
        },
        onFullscreen: function(){
            
        },
        onWindowed: function(){
            
        },
        onFileLoaded: function(files){
            
        }
    },
    Init: function(){
    
    }
}
//
//  Application Specific Code
//


App.Init = function(){
    App.Objects = {
        SS: null,
        Animation: null,
        Sprite: null
    };
    
    //Display 
    App.Display.EnableButton();
    
    //Windows
    App.Objects.Windows = {}
    
    //
    //  Sprite Sheet
    //
    App.Objects.Windows.ssWin  = App.Window.Create("Sprite Sheet Properties", "PC", false);
    App.Objects.Windows.ssWin.Hide.buttons();
    App.Objects.Windows.ssWin.elements.content.html().innerHTML = '<table>' + 
            '<tr>' +
            '   <td>SS Name:</td><td><input type="text" id="ssName"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>SS Path:</td><td><input type="text" id="ssPath"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>SS Width:</td><td><input type="text" id="ssWidth"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>SS Height:</td><td><input type="text" id="ssHeight"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>SS Rows:</td><td><input type="text" id="ssRows"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>SS Columns:</td><td><input type="text" id="ssColumns"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Cell Width:</td><td><input type="text" id="ssCellWidth"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Cell Height:</td><td><input type="text" id="ssCellHeight"></td>' +
            '</tr>' +
        '</table>' +
        '<button type="button" id="ssSave" onclick="App.SaveSS()">Save Sprite Sheet</button>' +
        '<button type="button" id="ssCancel" onclick="App.Objects.Windows.ssWin.Hide.window()">Cancel</button>';
            
    App.Objects.Windows.ssWin.Hide.window();
    
    App.SaveSS = function(e){
        
        App.Objects.SS = psl.Create.SpriteSheet(document.getElementById("ssName").value);
        
        App.Objects.SS.path = document.getElementById("ssPath").value;
        App.Objects.SS.width = document.getElementById("ssWidth").value;
        App.Objects.SS.height = document.getElementById("ssHeight").value;
        App.Objects.SS.rows = document.getElementById("ssRows").value;
        App.Objects.SS.columns = document.getElementById("ssColumns").value;
        App.Objects.SS.cellWidth = document.getElementById("ssCellWidth").value;
        App.Objects.SS.cellHeight = document.getElementById("ssCellHeight").value;
        App.Objects.SS.BuildClass();
        
        //Show Sprite Sheet Window to load properties
        App.Objects.Windows.ssWin.Hide.window();
    }
    
    //
    //  Animation
    //
    
    //Control Window
    App.Objects.Windows.aniWin = App.Window.Create("Animations", "PC", true);
    App.Objects.Windows.aniWin.elements.content.html().innerHTML ='<button type="button" id="animCreate" onclick="App.CreateAnimation()">Create Animation</button>';
    //
    //'<table>' + 
    //        '<tr>' +
    //        '   <td>Animations:</td><td><select id="animations" style="width: 100%" > </select></td>' +
    //        '</tr>' +
    //        '</table>' +
    //        '<button type="button" id="animCreate" onclick="App.CreateAnimation()">Create Animation</button>';
    
    //Creation Window
    App.Objects.Windows.createAnimWin = App.Window.Create('New Animation', "PC", false);
    App.Objects.Windows.createAnimWin.elements.content.html().innerHTML = '<table>' +
            '<tr>' +
            '   <td>Animation Name:</td><td><input type="text" id="animName"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Sprite Sheet Name:</td><td><input type="text" id="animSpriteSheet"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Number of Frames:</td><td><input type="text" id="animFrames"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Start Row:</td><td><input type="text" id="animStartRow"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Stop Column:</td><td><input type="text" id="animStartColumn"></td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Speed:</td><td><input type="text" id="animSpeed"></td>' +
            '</tr>' +
        '</table>' +
        '<button type="button" id="saveAnimation" onclick="App.SaveAnimation()">Save Animation</button>' +
        '<button type="button" id="cancelAnimation" onclick="App.Objects.Windows.createAnimWin.Hide.window();">Cancel</button>';
            
    App.Objects.Windows.createAnimWin.Hide.buttons();
    App.Objects.Windows.createAnimWin.Hide.window();
    
    App.CreateAnimation = function(){
        App.Objects.Windows.createAnimWin.Display.window();
        
        //Clear out Inputs
        document.getElementById("animName").value = "";
        document.getElementById("animSpriteSheet").value = "";
        document.getElementById("animFrames").value = "";
        document.getElementById("animStartRow").value = "";
        document.getElementById("animStartColumn").value = "";
        document.getElementById("animSpeed").value = "";
    }
    
    App.SaveAnimation = function(){
        App.Objects.Windows.createAnimWin.Hide.window();
        //Create Window
        
        
        //Pull Values from form
        var name        = document.getElementById("animName").value;
        var ss          = psl.Get.SpriteSheet(document.getElementById("animSpriteSheet").value);
        var numOfFrames = document.getElementById("animFrames").value | 0;
        var startRow    = document.getElementById("animStartRow").value | 0;
        var startColumn = document.getElementById("animStartColumn").value | 0;
        var speed       = document.getElementById("animSpeed").value | 0;
        
        //Create Animation
        var anim = psl.Create.Animation(name, numOfFrames, startRow, startColumn, ss);
        anim.speed = speed;
        
        var win = App.Window.Create(name, "PC", true);
        
        win.elements.content.html().innerHTML = '<table>' +
            '<tr>' +
            '   <td>Animation Name:</td><td>' + name + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Sprite Sheet Name:</td><td>' + ss.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Number of Frames:</td><td>' +  numOfFrames + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Start Row:</td><td>' +  startRow + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Start Column:</td><td>' +  startColumn + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td>Speed:</td><td>' +  speed + '</td>' +
            '</tr>' +
        '</table>';
        var btn = document.createElement("button");
        btn.type = "button";
        btn.innerHTML = "Delete Animation";
        btn.onclick = function(){
                App.DeleteAnimation(win);
            }
        win.elements.content.AppendChild(btn);

        
        //Load and Set Animation Active on Sprite
        App.Objects.Sprite.Animation.Load(anim);
        App.Objects.Sprite.Animation.SetActive(anim.name);
        
        //Call Animation Update
        App.Update();
    }
    App.DeleteAnimation = function(win){
        os.console.Comment("Removed Animation: " + win.Get.title());
        psl.Remove.Animation(win.Get.title());
        App.Window.UnDock(win);
        win.Hide.window();
    }
    
    //
    //  Sprite
    //
    
    App.Objects.Sprite = psl.Create.Sprite("sprite");
    App.Objects.Sprite.Move.ToPosition(20,20);
    App.Objects.Sprite.graphic.Append.ToHTML(App.HTML.Main.window);
    App.Events.onDropdown = function(win){
        if(win.Get.title() != "Animations"){
            os.console.Comment("Changing Animation To: " + win.Get.title());
            App.Objects.Sprite.Animation.SetActive(psl.Get.Animation(win.Get.title()).name);
        }
    }
    
    //File IO Demos
    App.FileIO.Add.Dropzone.App();
    App.FileIO.Add.FileSelector.Thumbnails("Select Image");
    
    App.Events.onFileLoaded = function(files){
        var extension;
        
        for(var i = 0; i < files.length; i++){
            extension = App.FileIO.Get.Extension(files[i]);
            
            if(extension == "PNG"){
                App.FileIO.Read.Image(files[i], function(img){
                    
                    
                    //Create Sprite Sheet
                    //App.Objects.SS = psl.Create.SpriteSheet("ss");
                    document.getElementById("ssName").value = "ss"; //App.Objects.SS.name;
                    
                    //App.Objects.SS.path = img.src;
                    document.getElementById("ssPath").value = App.FileIO.Read.ObjectURL(files[0]);
                    
                    //App.Objects.SS.width = img.width;
                    document.getElementById("ssWidth").value = img.width;
                    
                    //App.Objects.SS.height = img.height;
                    document.getElementById("ssHeight").value = img.height;
                    
                    //Show Sprite Sheet Window to load properties
                    App.Objects.Windows.ssWin.Display.window();
                    
                    //Attach to thumbnails to show user
                    img.style.width = "100px";
                    img.style.height = "100px";
                    App.HTML.Thumnails.window.appendChild(img);
                })
            }
            else{
                os.console.Warning("Unsupported image file format, must use PNG");
            }
        }
    }
    var lastUpdate = 0;
    var time;
    
    App.Update = function(){
        if(App.Objects.Sprite.Animation.Current.animation){
            time = psl.Get.Time()
            if((time - lastUpdate) > App.Objects.Sprite.Animation.Current.animation.speed){
                lastUpdate = time;
                
                // update animation
                App.Objects.Sprite.Animation.NextFrame();
                
                
            }
                window.requestAnimFrame(App.Update);
        }
        //window.webkitRequestAnimationFrame(Update);
    };
}


//Utility Functions, Classes and Objects
psl = (function(){
    //PRIVATE MODULE PROPERTIES
    var pInstance = null;
    
    //CONSTANTS
    var NAME = "PlayStyleLabs";
    var VERSION = "0.1";
    var PATH = "scripts/psl.js";
    var BROWSER = null;
    /**
    * Provides requestAnimationFrame in a cross browser way.
    */
   window.requestAnimFrame = (function() {
     return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
              window.setTimeout(callback, 1000/60);
            };
   })();
               
    //INTERFACE TO BIOLUCID MODULE
    var constructor = function(){

        //PRIVATE OBJECTS, PROPERTIES AND CLASS DEFINITIONS
        //
        
        
        //Animation Classes
        var CSprite = function(sName){
            var self = this;
            this.name = sName;
            this.graphic = pInstance.Create.HTML("div");
            this.graphic.html.id = sName;
            this.graphic.html.style.position = "absolute";
            this.Position ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Rotation ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Move = {
                Up: function(iDist){
                    self.Position.y -= iDist;
                    self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
                },
                Down: function(iDist){
                    self.Position.y += iDist;
                    self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
                },
                Left: function(iDist){
                    self.Position.x -= iDist;
                    self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                },
                Right: function(iDist){
                    self.Position.x += iDist;
                    self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                },
                ToPosition: function(iX, iY){
                    self.Position.x = iX;
                    self.Position.y = iY;
                    
                    if(self.graphic.html.style.webkitTransform != undefined)
                        self.graphic.html.style.webkitTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    else
                        self.graphic.html.style.MozTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    //self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                    //self.graphic.html.style.top  = (self.Position.y).toFixed(0) + "px";
                }
            }
            
            this.Rotate = function(deg){
                self.graphic.html.style.webkitTransform = "rotate("+ deg +"deg)";
            }
            
            this.Spin = {
                X: function(deg){
                    
                },
                Y: function(deg){
                    
                }
            }
            
            
            this.Animation = {
                list: pInstance.Create.Map(),
                Current: {
                    animation: null,
                    frames: 0,
                    index: 0
                },
                loop: false,
                lastUpdate: 0,
                NextFrame: function(){//Transtion to next animation frame
                    self.Animation.lastUpdate = pInstance.Get.Time();
                    
                    self.Animation.Current.index = self.Animation.Current.index == self.Animation.Current.animation.numOfFrames - 1 ? 0  : self.Animation.Current.index + 1;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[self.Animation.Current.index];

                    self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                },
                Update: function(){   //Transition to Next Frame, if ready
                    
                },
                Start: function(){    //Auto Animate
                    
                },
                Pause: function(){    //Pause Animation 
                    
                },
                Reset: function(){    //Reset Animation to First Frame
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = 0;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[0];
                },
                Load: function(oAnimation){     //Load New Animation into Sprite Object
                    self.Animation.list.put(oAnimation.name, oAnimation);
                },
                SetActive: function(sName){//Set Animation
                    //Remove Current Animation Class
                    if(self.Animation.Current.animation)
                        self.graphic.Remove.Class(self.Animation.Current.animation.sheet.GetClass() + " ");
                    
                    //Set new animation object
                    self.Animation.Current.animation = self.Animation.list.get(sName);
                    
                    //Add animation class to html
                    self.graphic.Append.Class(self.Animation.Current.animation.sheet.GetClass());
                    
                    //Update frame on graphic
                    self.Animation.SetFrame(0);
                },
                SetFrame: function(iFrameNumber){ //Set To Specific Frame in Current Animation
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = iFrameNumber;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[iFrameNumber];

                    self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                }
                
            }
        }
    
        var CSpriteSheet = function(sName){
            var self = this;
            this.width = 0;
            this.height = 0;
            this.cellWidth = 0;
            this.cellHeight = 0;
            this.rows = 0;
            this.columns = 0;
            this.path = "";
            this.id = 0;
            this.name = sName;
            var className = ""
            this.BuildClass = function(sClassName){
                className = sClassName ? sClassName : self.name;
    
                var style = "position: absolute; ";
                style += "overflow: hidden;";
                style += "background: url('" + this.path + "'); ";
                style += "width: " + this.cellWidth +"px;";
                style += "height: " + this.cellHeight + "px;";
                
                pInstance.Create.CSSClass("."+className, style);
            }
            this.GetClass = function(){
                return className;
            }
        }
        
        var CAnimation = function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet){
            var self = this;
            this.name = sName;
            this.sheet = oSpriteSheet;
            this.numOfFrames = iNumOfFrames;
            this.StartFrame = null;
            this.StopFrame  = null;
            this.frames = [];
            this.speed = 0;
            //Set Stop Column, Row and index
            
            //Populate frames array
            
             {
                var _frame = 0;
                
                //need to have initial starting point, will be reset to 0 afterwards
                var _iStartColumn = iStartColumn;
                
                // runs through rows (y) and columns (x) and adds them to the frames array
                for(var y = iStartRow; y < oSpriteSheet.rows && _frame < iNumOfFrames; y++){
                    for (var x = _iStartColumn; x < oSpriteSheet.columns && _frame < iNumOfFrames; x++,_frame++){
                        var fr = new CAnimationFrame(x*oSpriteSheet.cellWidth,y*oSpriteSheet.cellHeight,oSpriteSheet.cellWidth,oSpriteSheet.cellHeight);
                        fr.frame = _frame;
                        fr.row = y;
                        fr.column = x;
                        self.frames.push(fr);
                    }
                    // resetting starting column (x) to 0, so it goes through all following columns
                    _iStartColumn = 0;

                }
            }
        }
    
        var CAnimationFrame = function(iX, iY, iWidth, iHeight){
            this.x = iX;
            this.y = iY;
            this.width = iWidth;
            this.height = iHeight;
            this.frame = 0;         //Sprite Sheet Frame Number
            this.row = 0;
            this.column = 0;
        }
        
        //Base Classes
        var CHTMLElement = function(tag){
                this.html = null;
                this.classes = "";
                var self = this;
                
                        
                this.Append ={
                    Class: function(sClass){
                        self.classes += sClass + " ";
                        self.html.className = self.classes;
                        return self.classes;
                    },
                    Child: function(childHTML){
                        self.html.appendChild(childHTML);
                    },
                    ToID: function(parentID){
                        document.getElementById(parentID).appendChild(self.html);
                    },
                    ToHTML: function(parentHTML){
                        parentHTML.appendChild(self.html);
                    }
                }
                
                this.Remove = {
                    Class: function(sClass){
                        
                        self.classes = self.classes.replace(sClass, "");
                        
                        return self.classes;
                    },
                    Child: function(id){
                        self.html.removeChild(document.getElementById(id));
                    },
                    //removes all classes from class list
                    AllClasses: function(){
                        self.classes = "";
                        self.html.className = "";
                    },
                    AllChildren: function(){
                    
                        if ( self.html.hasChildNodes() )
                        {
                            while ( self.html.childNodes.length >= 1 )
                            {
                                self.html.removeChild( this.html.firstChild );       
                            } 
                        }
                    }
                    
                }
              
                this.SetHTML = function(oHTML){
                    self.html = oHTML;//document.getElementById(id);
                    //self.html = _html;
                    self.classes = this.html.className;
                }
                this.SetSelf = function(obj){
                    self = obj;
                }
                
                //Creates HTML element if tag exist        
                this.html =  tag ? document.createElement(tag) : null;
        
                return this;
            }
            
            
        //Map Object
        var CMap = function(linkEntries){
            this.current = undefined;
            this.size = 0;
            this.isLinked = true;
        
            if(linkEntries === false)
            {
                    this.disableLinking();
            }
                    
            this.from = function(obj, foreignKeys, linkEntries)
            {
                var map = new Map(linkEntries);
        
                for(var prop in obj) {
                        if(foreignKeys || obj.hasOwnProperty(prop))
                                map.put(prop, obj[prop]);
                }
        
                return map;
            }
            
            this.noop = function()
            {
                    return this;
            }
            
            this.illegal = function()
            {
                    throw new Error('can\'t do this with unlinked maps');
            }
            
            this.reverseIndexTableFrom = function(array, linkEntries)
            {
                var map = new Map(linkEntries);
        
                for(var i = 0, len = array.length; i < len; ++i) {
                        var	entry = array[i],
                                list = map.get(entry);
        
                        if(list) list.push(i);
                        else map.put(entry, [i]);
                }
        
                return map;
            }
        
            this.cross = function(map1, map2, func, thisArg)
            {
                var linkedMap, otherMap;
            
                if(map1.isLinked) {
                        linkedMap = map1;
                        otherMap = map2;
                }
                else if(map2.isLinked) {
                        linkedMap = map2;
                        otherMap = map1;
                }
                else Map.illegal();
            
                for(var i = linkedMap.size; i--; linkedMap.next()) {
                        var key = linkedMap.key();
                        if(otherMap.contains(key))
                                func.call(thisArg, key, map1.get(key), map2.get(key));
                }
            
                return thisArg;
            }
        
            this.uniqueArray = function(array)
            {
                    var map = new Map;
            
                    for(var i = 0, len = array.length; i < len; ++i)
                            map.put(array[i]);
            
                    return map.listKeys();
            }                                    
        };
        
        CMap.prototype.disableLinking = function(){
            this.isLinked = false;
            this.link = Map.noop;
            this.unlink = Map.noop;
            this.disableLinking = Map.noop;
            this.next = Map.illegal;
            this.key = Map.illegal;
            this.value = Map.illegal;
            this.removeAll = Map.illegal;
            this.each = Map.illegal;
            this.flip = Map.illegal;
            this.drop = Map.illegal;
            this.listKeys = Map.illegal;
            this.listValues = Map.illegal;
        
            return this;
        };
        
        CMap.prototype.hash = function(value){
            return value instanceof Object ? (value.__hash ||
                    (value.__hash = 'object ' + ++arguments.callee.current)) :
                    (typeof value) + ' ' + String(value);
        };
        
        CMap.prototype.hash.current = 0;            
        CMap.prototype.link = function(entry){
                if(this.size === 0) {
                        entry.prev = entry;
                        entry.next = entry;
                        this.current = entry;
                }
                else {
                        entry.prev = this.current.prev;
                        entry.prev.next = entry;
                        entry.next = this.current;
                        this.current.prev = entry;
                }
        };
        
        CMap.prototype.unlink = function(entry) {
                if(this.size === 0)
                        this.current = undefined;
                else {
                        entry.prev.next = entry.next;
                        entry.next.prev = entry.prev;
                        if(entry === this.current)
                                this.current = entry.next;
                }
        };
        
        CMap.prototype.get = function(key) {
                var entry = this[this.hash(key)];
                return typeof entry === 'undefined' ? undefined : entry.value;
        };
        
        CMap.prototype.put = function(key, value) {
                var hash = this.hash(key);
        
                if(this.hasOwnProperty(hash))
                        this[hash].value = value;
                else {
                        var entry = { key : key, value : value };
                        this[hash] = entry;
        
                        this.link(entry);
                        ++this.size;
                }
        
                return this;
        };
        
        CMap.prototype.remove = function(key) {
                var hash = this.hash(key);
        
                if(this.hasOwnProperty(hash)) {
                        --this.size;
                        this.unlink(this[hash]);
        
                        delete this[hash];
                }
        
                return this;
        };
        
        CMap.prototype.removeAll = function() {
                while(this.size)
                        this.remove(this.key());
        
                return this;
        };
        
        CMap.prototype.contains = function(key) {
                return this.hasOwnProperty(this.hash(key));
        };
       
        CMap.prototype.isUndefined = function(key) {
                var hash = this.hash(key);
                return this.hasOwnProperty(hash) ?
                        typeof this[hash] === 'undefined' : false;
        };
        
        CMap.prototype.next = function() {
                this.current = this.current.next;
        };
        
        CMap.prototype.key = function() {
                return this.current.key;
        };
        
        CMap.prototype.value = function() {
                return this.current.value;
        };
       
        CMap.prototype.each = function(func, thisArg) {
                if(typeof thisArg === 'undefined')
                        thisArg = this;
        
                for(var i = this.size; i--; this.next()) {
                        var n = func.call(thisArg, this.key(), this.value(), i > 0);
                        if(typeof n === 'number')
                                i += n; // allows to add/remove entries in func
                }
        
                return this;
        };
        
        CMap.prototype.flip = function(linkEntries) {
                var map = new Map(linkEntries);
        
                for(var i = this.size; i--; this.next()) {
                        var	value = this.value(),
                                list = map.get(value);
        
                        if(list) list.push(this.key());
                        else map.put(value, [this.key()]);
                }
        
                return map;
        };
        
        CMap.prototype.drop = function(func, thisArg) {
                if(typeof thisArg === 'undefined')
                        thisArg = this;
        
                for(var i = this.size; i--; ) {
                        if(func.call(thisArg, this.key(), this.value())) {
                                this.remove(this.key());
                                --i;
                        }
                        else this.next();
                }
        
                return this;
        };
        
        CMap.prototype.listValues = function() {
                var list = [];
        
                for(var i = this.size; i--; this.next())
                        list.push(this.value());
        
                return list;
        }
        
        CMap.prototype.listKeys = function() {
                var list = [];
        
                for(var i = this.size; i--; this.next())
                        list.push(this.key());
        
                return list;
        }
        
        CMap.prototype.toString = function() {
                var string = '[object Map';
        
                function addEntry(key, value, hasNext) {
                        string += '    { ' + this.hash(key) + ' : ' + value + ' }' +
                                (hasNext ? ',' : '') + '\n';
                }
        
                if(this.isLinked && this.size) {
                        string += '\n';
                        this.each(addEntry);
                }
        
                string += ']';
                return string;
        };
        
        
        
        //  Finite State Machine
        var CFiniteStateMachine = function(cEntity){
            var owner = cEntity;
            var _CurrentState = new CState();
            var self = this;
            var _Enter = function(oMessage){
                _CurrentState.Enter(owner, oMessage);
            }
            
            var _Exit = function(oMessage){
                _CurrentState.Exit(owner, oMessage);
            }
            
            var _Execute = function(oMessage){
                _CurrentState.Execute(owner, oMessage);
            }
            
            //Control Methods
            this.Transition = function(sName, oMessage){
                //Call Exit for Current State
                _Exit(oMessage);
                
                //Get New State to Transition Too
                _CurrentState = _States.get(sName);
                
                //Call New States Setup and Exectue Methods
                _Enter(oMessage);
                _Execute(oMessage);
            }
            this.SetState = function(sName){
                _CurrentState = _States.get(sName);
            }
            this.Update = function(oMessage){
                _Execute(oMessage);
            }
            this.GetState = function(){
                return _CurrentState.GetName();
            }
        }
        
        // States for FSM
        var CState = function(sName){
            var _name = sName;
            var self = this;
            
            this.Enter = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Exit = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Execute = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.GetName = function(){
                return _name;
            }

        }
        //Holds all states created
        // key: name, value: CState
        var _States = new CMap();
        
        
       
        
        var Animations = new CMap();
        var SpriteSheets = new CMap();
        var Sprites = new CMap();
        
        //PUBLIC OBJECTS/METHODS
        return{
            Get: {
                Sprite: function(sName){
                    return Sprites.get(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.get(sName)
                },
                Animation: function(sName){
                    return Animations.get(sName);
                },
                Time: function(){
                    return Date.now();
                },
                State: function(sName){
                    return _States.get(sName);  
                },
                Name: function(){
                    return NAME;
                },
                Version: function(){
                    return VERSION;
                },
                Path: function(){
                    return PATH;
                }
            },
            Remove: {
                Sprite: function(sName){
                    return Sprites.remove(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.remove(sName)
                },
                Animation: function(sName){
                    return Animations.remove(sName);
                }
            },
            
            Create: {
                HTML: function(tag){
                    return new CHTMLElement(tag);
                },
                Map: function(){
                    return new CMap();
                },
                FSM: function(cEntity){
                    return new CFiniteStateMachine(cEntity);
                    
                },
                State: function(sName){
                    var state = new CState(sName);
                    _States.put(sName, state);
                    return state;
                },
                Sprite: function(sName){
                    var sprite = new CSprite(sName);
                    Sprites.put(sName, sprite);
                    
                    return  sprite;
                },
                SpriteSheet: function(sName){
                    var sheet = new CSpriteSheet(sName);
                    SpriteSheets.put(sName, sheet);
                    
                    return sheet;   
                },
                Animation: function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet){
                    var ani = new CAnimation(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet);
                    Animations.put(sName, ani);
                    
                    return ani;
                },
                AnimationFrame: function(iX, iY, iWidth, iHeight){
                    return new CAnimationFrame(iX, iY, iWidth, iHeight);
                },
                CSSClass: function(sClass, sStyle) {
                    if (!document.styleSheets) {
                        return;
                    }
                
                    if (document.getElementsByTagName("head").length == 0) {
                        return;
                    }
                
                    var stylesheet;
                    var mediaType;
                    if (document.styleSheets.length > 0) {
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            var media = document.styleSheets[i].media;
                            mediaType = typeof media;
                
                            if (mediaType == "string") {
                                if (media == "" || (media.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            } else if (mediaType == "object") {
                                if (media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            }
                
                            if (typeof styleSheet != "undefined") {
                                break;
                            }
                        }
                    }
                
                    if (typeof styleSheet == "undefined") {
                        var styleSheetElement = document.createElement("style");
                        styleSheetElement.type = "text/css";
                
                        document.getElementsByTagName("head")[0].appendChild(styleSheetElement);
                
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            styleSheet = document.styleSheets[i];
                        }
                
                        var media = styleSheet.media;
                        mediaType = typeof media;
                    }
                
                    if (mediaType == "string") {
                        for (i = 0; i < styleSheet.rules.length; i++) {
                            if (styleSheet.rules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.rules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.addRule(sClass, sStyle);
                    }
                    else if (mediaType == "object") {
                        for (i = 0; i < styleSheet.cssRules.length; i++) {
                            if (styleSheet.cssRules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.cssRules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.insertRule(sClass + "{" + sStyle + "}", 0);
                    }
                }
                
            },
            Modules:{
                
            },
            CHTMLElement:  function(tag){
                this.html = null;
                this.classes = "";
                var self = this;
                
                        
                this.Append ={
                    Class: function(sClass){
                        self.classes += sClass + " ";
                        self.html.className = self.classes;
                        return self.classes;
                    },
                    Child: function(childHTML){
                        self.html.appendChild(childHTML);
                    },
                    ToID: function(parentID){
                        document.getElementById(parentID).appendChild(self.html);
                    },
                    ToHTML: function(parentHTML){
                        parentHTML.appendChild(self.html);
                    }
                }
                
                this.Remove = {
                    Class: function(sClass){
                        
                        self.classes = self.classes.replace(sClass, "");
                        
                        return self.classes;
                    },
                    Child: function(id){
                        self.html.removeChild(document.getElementById(id));
                    },
                    //removes all classes from class list
                    AllClasses: function(){
                        self.classes = "";
                        self.html.className = "";
                    },
                    AllChildren: function(){
                    
                        if ( self.html.hasChildNodes() )
                        {
                            while ( self.html.childNodes.length >= 1 )
                            {
                                self.html.removeChild( this.html.firstChild );       
                            } 
                        }
                    }
                    
                }
              
                this.SetHTML = function(oHTML){
                    self.html = oHTML;//document.getElementById(id);
                    //self.html = _html;
                    self.classes = this.html.className;
                }
                this.SetSelf = function(obj){
                    self = obj;
                }
                
                //Creates HTML element if tag exist        
                this.html =  tag ? document.createElement(tag) : null;
        
                return this;
            }
                
        }
    }
    return {
        //Instantiates Objects and Returns
        //  Insures a single object
        Instance: function()
        {
            if(!pInstance)
            {
                //Instantiate if pInstance does not exist
                pInstance = constructor();
            }
            
            return pInstance;
        }
    }
})().Instance();
